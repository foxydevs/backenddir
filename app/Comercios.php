<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Comercios extends Model
{
    protected $table = 'comercios';

    public function anuncios(){
        return $this->hasMany('App\Anuncios','comercio');
    }
}
