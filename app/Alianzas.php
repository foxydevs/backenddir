<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Alianzas extends Model
{
    protected $table = 'alianzas';

    public function comercios(){
        return $this->hasOne('App\Comercios','id','comercio');
    }

    public function alianzas(){
        return $this->hasOne('App\Comercios','id','alianza');
    }
}
