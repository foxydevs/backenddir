<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Alianzas;
use Response;
use Validator;
class AlianzasController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Alianzas::all(), 200);
    }
    
    public function getThisByFilter(Request $request,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'comercio':{
                    $objectSee = Alianzas::whereRaw('comercio=?',[$state])->with('comercios','alianzas')->get();
                    break;
                }
                case 'alianza':{
                    $objectSee = Alianzas::whereRaw('alianza=?',[$state])->with('comercios','alianzas')->get();
                    break;
                }
                default:{
                    $objectSee = Alianzas::with('comercios','alianzas')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Alianzas::with('comercios','alianzas')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comercio'          => 'required',
            'alianza'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Alianzas();
                $newObject->color            = $request->get('color');
                $newObject->fondo            = $request->get('fondo');
                $newObject->link            = $request->get('link');
                $newObject->state            = $request->get('state');
                $newObject->tipo            = $request->get('tipo');
                $newObject->latitud            = $request->get('latitud');
                $newObject->longitud            = $request->get('longitud');
                $newObject->date            = $request->get('date');
                $newObject->time            = $request->get('time');
                $newObject->comercio            = $request->get('comercio');
                $newObject->alianza            = $request->get('alianza');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Alianzas::find($id);
        if ($objectSee) {
            $objectSee->comercios;
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }

    public function add(Request $request)
    {
        $comercio = $request->get('comercio');
        $alianza = $request->get('alianza');
        $UserEvent  = Alianzas::whereRaw("comercio = ? AND alianza = ?", [$comercio,$alianza])->first();
        if(!$UserEvent){
            $validator = Validator::make($request->all(), [
                'state'          => 'required',
                'comercio'          => 'required',
                'alianza'          => 'required'
            ]);
            if ( $validator->fails() ) {
                $returnData = array (
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $newObject = new Alianzas();
                    $newObject->state            = $request->get('state');
                    $newObject->comercio            = $comercio;
                    $newObject->alianza            = $alianza;
                    $newObject->save();
                    return Response::json($newObject, 200);
                
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
                
            }
        } else {
            $UserEvent  = Alianzas::whereRaw("comercio = ? AND alianza = ?", [$comercio,$alianza])->first();
            if ($UserEvent) {
                try {
                    $UserEvent->state = $request->get('state', $UserEvent->state);
            
                    $UserEvent->save();
                    return Response::json($UserEvent, 200);
                } catch (Exception $e) {
                    $returnData = array (
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                    return Response::json($returnData, 500);
                }
            }
            else {
                $returnData = array (
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        }
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Alianzas::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->color = $request->get('color', $objectUpdate->color);
                $objectUpdate->fondo = $request->get('fondo', $objectUpdate->fondo);
                $objectUpdate->link = $request->get('link', $objectUpdate->link);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->latitud = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->date = $request->get('date', $objectUpdate->date);
                $objectUpdate->time = $request->get('time', $objectUpdate->time);
                $objectUpdate->comercio = $request->get('comercio', $objectUpdate->comercio);
                $objectUpdate->alianza = $request->get('alianza', $objectUpdate->alianza);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Alianzas::find($id);
        if ($objectDelete) {
            try {
                Alianzas::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
