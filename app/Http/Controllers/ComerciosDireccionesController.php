<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ComerciosDirecciones;
use Response;
use Validator;

class ComerciosDireccionesController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(ComerciosDirecciones::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'comercio':{
                    $objectSee = ComerciosDirecciones::whereRaw('comercio=?',[$state])->get();
                    break;
                }
                case 'tipo':{
                    $objectSee = ComerciosDirecciones::whereRaw('tipo=?',[$state])->get();
                    break;
                }
                default:{
                    $objectSee = ComerciosDirecciones::whereRaw('state=?',[$state])->get();
                    break;
                }
    
            }
        }else{
            $objectSee = ComerciosDirecciones::all();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'direccion'          => 'required',
            'comercio'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new ComerciosDirecciones();
                $newObject->nombre            = $request->get('nombre');
                $newObject->direccion            = $request->get('direccion');
                $newObject->calle            = $request->get('calle');
                $newObject->ciudad            = $request->get('ciudad');
                $newObject->codigo            = $request->get('codigo');
                $newObject->description            = $request->get('description');
                $newObject->foto            = $request->get('foto');
                $newObject->color            = $request->get('color');
                $newObject->fondo            = $request->get('fondo');
                $newObject->link            = $request->get('link');
                $newObject->state            = $request->get('state');
                $newObject->tipo            = $request->get('tipo');
                $newObject->latitud            = $request->get('latitud');
                $newObject->longitud            = $request->get('longitud');
                $newObject->comercio            = $request->get('comercio');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = ComerciosDirecciones::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = ComerciosDirecciones::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->direccion = $request->get('direccion', $objectUpdate->direccion);
                $objectUpdate->calle = $request->get('calle', $objectUpdate->calle);
                $objectUpdate->ciudad = $request->get('ciudad', $objectUpdate->ciudad);
                $objectUpdate->codigo = $request->get('codigo', $objectUpdate->codigo);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->foto = $request->get('foto', $objectUpdate->foto);
                $objectUpdate->color = $request->get('color', $objectUpdate->color);
                $objectUpdate->fondo = $request->get('fondo', $objectUpdate->fondo);
                $objectUpdate->link = $request->get('link', $objectUpdate->link);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->latitud = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->comercio = $request->get('comercio', $objectUpdate->comercio);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = ComerciosDirecciones::find($id);
        if ($objectDelete) {
            try {
                ComerciosDirecciones::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
