<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\AnunciosUsuarios;
use Response;
use Validator;
class AnunciosUsuariosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(AnunciosUsuarios::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'usuario':{
                    $objectSee = AnunciosUsuarios::whereRaw('usuario=?',[$state])->with('user')->get();
                    break;
                }
                case 'anuncio':{
                    $objectSee = AnunciosUsuarios::whereRaw('anuncio=?',[$state])->with('user')->get();
                    break;
                }
                default:{
                    $objectSee = AnunciosUsuarios::whereRaw('usuario=? and anuncio=?',[$id,$state])->with('user')->get();
                    break;
                }
    
            }
        }else{
            $objectSee = AnunciosUsuarios::whereRaw('usuario=? and anuncio=?',[$id,$state])->with('user')->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'usuario'          => 'required',
            'anuncio'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new AnunciosUsuarios();
                $newObject->color            = $request->get('color');
                $newObject->fondo            = $request->get('fondo');
                $newObject->link            = $request->get('link');
                $newObject->state            = $request->get('state');
                $newObject->tipo            = $request->get('tipo');
                $newObject->latitud            = $request->get('latitud');
                $newObject->longitud            = $request->get('longitud');
                $newObject->date            = $request->get('date');
                $newObject->time            = $request->get('time');
                $newObject->usuario            = $request->get('usuario');
                $newObject->anuncio            = $request->get('anuncio');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = AnunciosUsuarios::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = AnunciosUsuarios::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->color            = $request->get('color', $objectUpdate->color);
                $objectUpdate->fondo            = $request->get('fondo', $objectUpdate->fondo);
                $objectUpdate->link            = $request->get('link', $objectUpdate->link);
                $objectUpdate->state            = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo            = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->latitud            = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud            = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->date            = $request->get('date', $objectUpdate->date);
                $objectUpdate->time            = $request->get('time', $objectUpdate->time);
                $objectUpdate->usuario            = $request->get('usuario', $objectUpdate->usuario);
                $objectUpdate->anuncio            = $request->get('anuncio', $objectUpdate->anuncio);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = AnunciosUsuarios::find($id);
        if ($objectDelete) {
            try {
                AnunciosUsuarios::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
