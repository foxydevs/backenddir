<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Users;
use App\Events;
use App\UserEvents;
use App\UsersClients;
use Response;
use Validator;
use Hash;
use Storage;
use DB;
use Auth;

class AuthenticateController extends Controller
{
    public function login(Request $request) {
        $validator = Validator::make($request->all(), [
            'username'  => 'required',
            'password'  => 'required'
        ]);

        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {

                $userdata = array(
                    'username'  => $request->get('username'),
                    'password'  => $request->get('password')
                );

                if(Auth::attempt($userdata, true)) {
                    $user = Users::find(Auth::user()->id);
                    $user->last_conection = date('Y-m-d H:i:s');
                    $user->one_signal_id = $request->get('one_signal_id', $user->one_signal_id);
                    $user->save();
                    return Response::json($user, 200);
                }
                else {
                    $returnData = array (
                        'status' => 401,
                        'message' => 'No valid Username or Password'
                    );
                    return Response::json($returnData, 401);
                }

                return Response::json($newObject, 200);

            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }


    public function upload(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'avatar'      => 'required|image|mimes:jpeg,png,jpg'
        ]);
    
        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
    
                $path = Storage::disk('s3')->put($request->carpeta, $request->avatar);
                return Response::json(Storage::disk('s3')->url($path), 200);
    
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
    
        }
    }

    
    public function logout(Request $request) {
        return 'adios';
    }
}
