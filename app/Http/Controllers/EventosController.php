<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Eventos;
use Response;
use Validator;
use DB;

class EventosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $nowD = date('Y-m-d');
        $nowT = date('H:i:s');
        // $nowD = "2018-09-25";
        // $nowT = "21:08:21";
        return Response::json(Events::whereRaw('date>=? or (date>=? and time>=?)',[$nowD,$nowD,$nowT])->orderby('date')->orderby('time')->get(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Eventos::whereRaw('state=?',[$id,$state])->get();
                    break;
                }
                case 'comercio':{
                    $objectSee = Eventos::whereRaw('comercio=?',[$id,$state])->get();
                    break;
                }
                default:{
                    $objectSee = Eventos::whereRaw('state=?',[$id,$state])->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Eventos::all()->get();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function eventsNear(Request $request){
        try {
            $lat = $request->get('latitud');
            $lon = $request->get('longitud');
            $distance = $request->get('distance');
            $box = $this->getBoundaries($lat, $lon, $distance);

            $eventsNear = DB::table('eventos')->select(DB::raw('id,date,time, (6371 * ACOS( 
                                            SIN(RADIANS(latitud)) 
                                            * SIN(RADIANS(' . $lat . ')) 
                                            + COS(RADIANS(longitud - ' . $lon . ')) 
                                            * COS(RADIANS(latitud)) 
                                            * COS(RADIANS(' . $lat . '))
                                            )
                               ) AS distance'))->whereRaw('((latitud BETWEEN ? AND ?) AND (longitud BETWEEN ? AND ?)) HAVING distance < ? ', 
                               [$box['min_lat'], $box['max_lat'], $box['min_lng'], $box['max_lng'], $distance])->get();
            
            $eventsId = collect();
            foreach ($eventsNear as $key => $value) {
                $eventsId->push($value->id);
            }

            $eventsOnDate = Events::whereIn('id', $eventsId)->whereRaw('CONCAT(date, " ", time) > ?', [date('Y-m-d H:m:s')] )->orderby('date','asc')->orderby('time','asc')->get();

            return Response::json($eventsOnDate, 200);
        } catch (Exception $e) {
            $returnData = array (
                'status' => 500,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 500);
        }
    }
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'comercio'          => 'required',
            'nombre'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Eventos();
                $newObject->nombre            = $request->get('nombre');
                $newObject->description            = $request->get('description');
                $newObject->portada            = $request->get('portada');
                $newObject->foto1            = $request->get('foto1');
                $newObject->foto2            = $request->get('foto2');
                $newObject->foto3            = $request->get('foto3');
                $newObject->color            = $request->get('color');
                $newObject->fondo            = $request->get('fondo');
                $newObject->link            = $request->get('link');
                $newObject->state            = $request->get('state');
                $newObject->tipo            = $request->get('tipo');
                $newObject->latitud            = $request->get('latitud');
                $newObject->longitud            = $request->get('longitud');
                $newObject->date            = $request->get('date');
                $newObject->time            = $request->get('time');
                $newObject->comercio            = $request->get('comercio');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Eventos::find($id);
        if ($objectSee) {
            
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Eventos::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->portada = $request->get('portada', $objectUpdate->portada);
                $objectUpdate->foto1 = $request->get('foto1', $objectUpdate->foto1);
                $objectUpdate->foto2 = $request->get('foto2', $objectUpdate->foto2);
                $objectUpdate->foto3 = $request->get('foto3', $objectUpdate->foto3);
                $objectUpdate->color = $request->get('color', $objectUpdate->color);
                $objectUpdate->fondo = $request->get('fondo', $objectUpdate->fondo);
                $objectUpdate->link = $request->get('link', $objectUpdate->link);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->latitud = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->date = $request->get('date', $objectUpdate->date);
                $objectUpdate->time = $request->get('time', $objectUpdate->time);
                $objectUpdate->comercio = $request->get('comercio', $objectUpdate->comercio);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Eventos::find($id);
        if ($objectDelete) {
            try {
                Eventos::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }


    function getBoundaries($lat, $lng, $distance = 1, $earthRadius = 6371)
    {
        $return = array();
        
        // Los angulos para cada dirección
        $cardinalCoords = array('north' => '0',
                                'south' => '180',
                                'east' => '90',
                                'west' => '270');
        $rLat = deg2rad($lat);
        $rLng = deg2rad($lng);
        $rAngDist = $distance/$earthRadius;
        foreach ($cardinalCoords as $name => $angle)
        {
            $rAngle = deg2rad($angle);
            $rLatB = asin(sin($rLat) * cos($rAngDist) + cos($rLat) * sin($rAngDist) * cos($rAngle));
            $rLonB = $rLng + atan2(sin($rAngle) * sin($rAngDist) * cos($rLat), cos($rAngDist) - sin($rLat) * sin($rLatB));
            $return[$name] = array('lat' => (float) rad2deg($rLatB), 
                                    'lng' => (float) rad2deg($rLonB));
        }
        return array('min_lat'  => $return['south']['lat'],
                    'max_lat' => $return['north']['lat'],
                    'min_lng' => $return['west']['lng'],
                    'max_lng' => $return['east']['lng']);
    }
    function distance($lat1, $lon1, $lat2, $lon2, $unit) {

        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) +  cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
            } else {
                return $miles;
            }
    }
    
}
