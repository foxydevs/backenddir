<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Comercios;
use App\Anuncios;
use Response;
use Validator;

class AnunciosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Anuncios::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Anuncios::whereRaw('state=?',[$state])->get();
                    break;
                }
                case 'comercio':{
                    $objectSee = Anuncios::whereRaw('comercio=?',[$state])->get();
                    break;
                }
                case 'category':{
                    $objectSee = Comercios::select('id')->whereRaw('categoria=?',[$state])->get();
                    $objectSee = Anuncios::whereIn('comercio',$objectSee)->get();
                    break;
                }
                case 'sub_categoria':{
                    $objectSee = Comercios::select('id')->whereRaw('sub_categoria=?',[$state])->get();
                    $objectSee = Anuncios::whereIn('comercio',$objectSee)->get();
                    break;
                }
                default:{
                    $objectSee = Anuncios::whereRaw('state=?',[$state])->get();
                    break;
                }
    
            }
        }else{
            $objectSee = Anuncios::all();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'nombre'          => 'required',
            'comercio'          => 'required',
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            try {
                $newObject = new Anuncios();
                $newObject->nombre            = $request->get('nombre');
                $newObject->description            = $request->get('description');
                $newObject->portada            = $request->get('portada');
                $newObject->foto1            = $request->get('foto1');
                $newObject->foto2            = $request->get('foto2');
                $newObject->foto3            = $request->get('foto3');
                $newObject->color            = $request->get('color');
                $newObject->fondo            = $request->get('fondo');
                $newObject->link            = $request->get('link');
                $newObject->state            = $request->get('state');
                $newObject->tipo            = $request->get('tipo');
                $newObject->latitud            = $request->get('latitud');
                $newObject->longitud            = $request->get('longitud');
                $newObject->date            = $request->get('date');
                $newObject->time            = $request->get('time');
                $newObject->comercio            = $request->get('comercio');
                $newObject->save();
                return Response::json($newObject, 200);
    
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Anuncios::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Anuncios::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->nombre = $request->get('nombre', $objectUpdate->nombre);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->portada = $request->get('portada', $objectUpdate->portada);
                $objectUpdate->foto1 = $request->get('foto1', $objectUpdate->foto1);
                $objectUpdate->foto2 = $request->get('foto2', $objectUpdate->foto2);
                $objectUpdate->foto3 = $request->get('foto3', $objectUpdate->foto3);
                $objectUpdate->color = $request->get('color', $objectUpdate->color);
                $objectUpdate->fondo = $request->get('fondo', $objectUpdate->fondo);
                $objectUpdate->link = $request->get('link', $objectUpdate->link);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->latitud = $request->get('latitud', $objectUpdate->latitud);
                $objectUpdate->longitud = $request->get('longitud', $objectUpdate->longitud);
                $objectUpdate->date = $request->get('date', $objectUpdate->date);
                $objectUpdate->time = $request->get('time', $objectUpdate->time);
                $objectUpdate->comercio = $request->get('comercio', $objectUpdate->comercio);
    
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Anuncios::find($id);
        if ($objectDelete) {
            try {
                Anuncios::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
