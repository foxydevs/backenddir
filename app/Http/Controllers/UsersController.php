<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Users;
use App\Accesos;
use App\Events;
use App\UserEvents;
use App\Friends;
use App\UsersClients;
use App\Staff;
use Faker\Factory as Faker;
use Response;
use Validator;
use Hash;
use Storage;
use DB;


class UsersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->has('type')) {
            if ($request->get('type') === 'admin') {
                return Response::json(Users::where('admin','=','1')->get(), 200);
            } else {
                return Response::json(Users::with('types','direcciones','membresias','referencias')->get(), 200);    
            }
        } else {
            return Response::json(Users::with('types','direcciones','membresias','referencias')->get(), 200);
        }
    }



    public function getThisByState($id,$state)
    {
        $objectSee = Users::whereRaw('nivelMembresia=?',[$state])->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }



    public function getThisByCode($id)
    {
        $objectSee = Users::whereRaw('opcion15=?',[$id])->with('types','direcciones','membresias','referencias')->get();
        if ($objectSee) {
            return Response::json($objectSee, 200);
        
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }


    public function getUsersOnly()
    {
        return Response::json(Users::where('type','=','1')->where('state','=','1')->with('types','direcciones','membresias','referencias')->get(), 200);
    }

    public function getUsersOnlyMine($id)
    {
        $objectSee = UsersClients::select('user')->whereRaw('client=?',[$id])->get();
        if ($objectSee) {

            return Response::json(Users::whereIn('id',$objectSee)->where('state','=','1')->with('types','direcciones','membresias','referencias')->get(), 200);

        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getClientsOnlyMine($id)
    {
        $objectSee = UsersClients::select('client')->whereRaw('user=?',[$id])->get();
        if ($objectSee) {
           
            $staff = Staff::whereRaw('app=?',$id)->get();
            $objectSee1 = Users::whereIn('id',$objectSee)->where('state','=','1')->with('types','direcciones','membresias','referencias')->get();
            foreach ($objectSee1 as $key => $value) {
                    $value->staff = 0;
                    $value->idStaff = 0;
                foreach ($staff as $key2 => $value2) {
                    if($value->id==$value2->user){
                        $value->staff = 1;
                        $value->idStaff = $value2->id;
                    }
                }
            }
            return Response::json($objectSee1, 200);

        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function getClientsOnly()
    {
        return Response::json(Users::where('type','=','2')->where('state','=','1')->with('types','direcciones','membresias','referencias')->get(), 200);
    }

    public function getUsersPendingOnly()
    {
        return Response::json(Users::where('state','=','21')->with('types','direcciones','membresias','referencias')->get(), 200);
    }

    public function getEnterpriseOnly()
    {
        return Response::json(Users::where('type','=','3')->where('state','=','1')->with('types','direcciones','membresias','referencias')->get(), 200);
    }

    public function findUserByName(Request $request) {
        if ($request->has('name')) {
            $name = $request->get('name');
            $users = Users::whereRaw('firstname LIKE ? OR lastname LIKE ?', ["%".$name."%", "%".$name."%"])->get();
            return Response::json($users, 200);    
        } else {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters'
            );
            return Response::json($returnData, 400);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     *'password'      => 'required|min:3|regex:/^.*(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[!-,:-@]).*$/',
     */
     public function store(Request $request)
     {
         $validator = Validator::make($request->all(), [
             'username'      => 'required',
             'password'      => 'required|min:3',
             'email'         => 'required|email'
         ]);
         
 
         if ($validator->fails()) {
             $returnData = array(
                 'status' => 400,
                 'message' => 'Invalid Parameters',
                 'validator' => $validator->messages()->toJson()
             );
             return Response::json($returnData, 400);
         }
         else {
             $email = $request->get('email');
             $email_exists  = Users::whereRaw("email = ?", $email)->count();
             $user = $request->get('username');
             $user_exists  = Users::whereRaw("username = ?", $user)->count();
             if($email_exists == 0 && $user_exists == 0){    
                 try {
                     $newObject = new Users();
                     $newObject->username = $request->get('username');
                     $newObject->password = Hash::make($request->get('password'));
                     $newObject->email = $email;
                     $newObject->firstname = $request->get('firstname', '');
                     $newObject->lastname = $request->get('lastname', '');
                     $newObject->work = $request->get('work', '');
                     $newObject->description = $request->get('description', '');
                     $newObject->age = $request->get('age', '');
                     $newObject->birthday = $request->get('birthday', '');
                     $newObject->phone = $request->get('phone', '');;
                     $newObject->admin = $request->get('admin', 0);
                     $newObject->pic1 = $request->get('pic1', 'http://foxylabs.xyz/Documentos/imgs/Zapatos.jpg');
                     $newObject->pic2 = $request->get('pic2', 'http://foxylabs.xyz/Documentos/imgs/Category.jpg');
                     $newObject->pic3 = $request->get('pic3', 'http://foxylabs.xyz/Documentos/imgs/categories.jpg');
                     $newObject->last_conection = date('Y-m-d H:m:s');
                     $newObject->facebook_id = '';
                     $newObject->one_signal_id = $request->get('one_signal_id','');
                     $newObject->youtube_channel = $request->get('youtube_channel','');
                     $newObject->referencia = $request->get('referencia',null);
                     $newObject->last_latitud = 0.0;
                     $newObject->last_longitud = 0.0;
                     $newObject->picture = "http://foxylabs.xyz/Documentos/imgs/logo.png";
                     $newObject->type = $request->get('type',2);
                     $newObject->opcion4 = $request->get('opcion4', null);
                     $newObject->opcion5 = $request->get('opcion5', null);
                     $newObject->opcion6 = $request->get('opcion6', null);
                     $newObject->opcion7 = $request->get('opcion7', null);
                     $newObject->opcion8 = $request->get('opcion8', null);
                     $newObject->opcion9 = $request->get('opcion9', null);
                     $newObject->opcion11 = $request->get('opcion11', null);
                     $newObject->opcion12 = $request->get('opcion12', null);
                     $newObject->opcion13 = $request->get('opcion13', null);
                     $newObject->opcion14 = $request->get('opcion14', null);
                     $newObject->opcion15 = $request->get('opcion15', null);
                     $newObject->opcion16 = $request->get('opcion16', null);
                     $newObject->opcion17 = $request->get('opcion17', null);
                     $newObject->opcion18 = $request->get('opcion18', null);
                     $newObject->opcion19 = $request->get('opcion19', null);
                     $newObject->opcion20 = $request->get('opcion20', null);
                     if($request->get('usuario')){
                        $newObject->state = $request->get('state',1);
                     }else{
                        $newObject->state = $request->get('state',21);
                     }
                     $newObject->save();
                     if($request->get('usuario')){
                        $newObject2 = new UsersClients();
                        $newObject2->client            = $newObject->id;
                        $newObject2->user              = $request->get('usuario');
                        $newObject2->save();
                        
                     }
                     elseif($newObject->type != "2"){
                        try {
                            $i=1;
                            while($i<=24)
                            {   
                                if($i<=24)
                                {
                                    $newAccess = new Accesos();
                                    $newAccess->agregar            = 1;
                                    $newAccess->eliminar           = 1;
                                    $newAccess->modificar          = 1;
                                    $newAccess->mostrar            = 1;
                                    $newAccess->usuario            = $newObject->id;
                                    $newAccess->modulo             = $i;
                                    $newAccess->save();
                                }
                                $i++;
                            }
                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                     }
                     $objectSee = Users::whereRaw('id=?',$newObject->id)->with('types','direcciones','membresias','referencias')->first();
                     if ($objectSee) {
                        Mail::send('emails.confirm', ['empresa' => 'GTechnology', 'url' => 'http://gtechnology.gt', 'app' => 'http://me.gtechnology.gt', 'password' => $request->get('password'), 'username' => $objectSee->username, 'email' => $objectSee->email, 'name' => $objectSee->firstname.' '.$objectSee->lastname,], function (Message $message) use ($objectSee){
                            $message->from('info@foxylabs.gt', 'Info GTechnology')
                                    ->sender('info@foxylabs.gt', 'Info GTechnology')
                                    ->to($objectSee->email, $objectSee->firstname.' '.$objectSee->lastname)
                                    ->replyTo('info@foxylabs.gt', 'Info GTechnology')
                                    ->subject('Usuario Creado');
                        
                        });
                         return Response::json($objectSee, 200);
                     
                     }
                     else {
                         $returnData = array (
                             'status' => 404,
                             'message' => 'No record found'
                         );
                         return Response::json($returnData, 404);
                     }
                     
                 }
                 catch(Exception $e) {
                     $returnData = array(
                         'status' => 500,
                         'message' => $e->getMessage()
                     );
                     return Response::json($returnData, 500);
                 }
             } else {
                if($email_exists>0){
                    $objectSee  = Users::whereRaw("email = ?", $email)->first();
                }else{
                    $objectSee  = Users::whereRaw("username = ?", $user)->first();
                }
                $objectUpdate = UsersClients::whereRaw('client=? and user=?',[$objectSee->id,$request->get('usuario')])->first();
                if ($objectUpdate) {
                    $returnData = array (
                        'status' => 400,
                        'message' => "User already exists"
                    );
                    return Response::json($returnData, 400);
                }
                else {
                    try {
                        $newObject = new UsersClients();
                        $newObject->client            = $objectSee->id;
                        $newObject->user              = $request->get('usuario');
                        $newObject->save();
                        return Response::json($objectSee, 200);
                    
                    } catch (\Illuminate\Database\QueryException $e) {
                        if($e->errorInfo[0] == '01000'){
                            $errorMessage = "Error Constraint";
                        }  else {
                            $errorMessage = $e->getMessage();
                        }
                        $returnData = array (
                            'status' => 505,
                            'SQLState' => $e->errorInfo[0],
                            'message' => $errorMessage
                        );
                        return Response::json($returnData, 500);
                    } catch (Exception $e) {
                        $returnData = array (
                            'status' => 500,
                            'message' => $e->getMessage()
                        );
                        return Response::json($returnData, 500);
                    }
                } 
                $returnData = array(
                     'status' => 400,
                     'message' => 'User already exists',
                     'validator' => $validator->messages()->toJson()
                 );
                 return Response::json($returnData, 400);
             }
         }
     }

     public function storeFcebook(Request $request)
     {
         $objectSee = Users::where('facebook_id',$request->get('facebook_id'))->first();
         if ($objectSee) {
             
             return Response::json($objectSee, 200);
         
         }
         else {
            $validator = Validator::make($request->all(), [
                'username'      => 'required',
                'email'         => 'required|email',
                'one_signal_id' => 'required',
                'facebook_id' => 'required'
            ]);
            
    
            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                $email = $request->get('email');
                $email_exists  = Users::whereRaw("email = ?", $email)->count();
                if($email_exists == 0){    
                    try {
                        $newObject = new Users();
                        $newObject->username = $request->get('facebook_id');
                        $newObject->password = '';
                        $newObject->email = $email;
                        $newObject->firstname = $request->get('firstname');
                        $newObject->lastname = $request->get('lastname');
                        $newObject->work = '';
                        $newObject->description = '';
                        $newObject->age = 0;
                        $newObject->birthday = null;
                        $newObject->phone = '';
                        $newObject->last_conection = date('Y-m-d H:m:s');
                        $newObject->facebook_id = $request->get('facebook_id');
                        $newObject->one_signal_id = $request->get('one_signal_id');
                        $newObject->last_latitud = 0.0;
                        $newObject->last_longitud = 0.0;
                        $newObject->picture = $request->get('picture');
                        $newObject->type = $request->get('type');
                        $newObject->save();

                        $token = JWTAuth::fromUser($newObject); 
                        $newObject->token = $token;

                        return Response::json($newObject, 200);
                    }
                    catch(Exception $e) {
                        $returnData = array(
                            'status' => 500,
                            'message' => $e->getMessage()
                        );
                        return Response::json($returnData, 500);
                    }
                } else {
                    $objectUpdate = Users::where('email',$email)->first();
                    if ($objectUpdate) {
                        try {

                            $objectUpdate->facebook_id = $request->get('facebook_id', $objectUpdate->facebook_id);
                            $objectUpdate->last_conection = date('Y-m-d H:m:s');
                            $objectUpdate->save();

                            $token = JWTAuth::fromUser($objectUpdate); 
                            $objectUpdate->token = $token;

                            return Response::json($objectUpdate, 200);

                        } catch (Exception $e) {
                            $returnData = array (
                                'status' => 500,
                                'message' => $e->getMessage()
                            );
                            return Response::json($returnData, 500);
                        }
                    }
                    else {
                        $returnData = array (
                            'status' => 404,
                            'message' => 'No record found'
                        );
                        return Response::json($returnData, 404);
                    }
                }
            } 
         }

         
     }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $objectSee = Users::whereRaw('id=?',$id)->with('types','direcciones','membresias','referencias')->first();
        if ($objectSee) {
            $objectSee->number_events_created = Events::where('user_created', '=', $objectSee->id)->count();
            $objectSee->number_events_assisted = UserEvents::whereRaw('user = ? AND state = 1', [$objectSee->id])->count();
            $objectSee->interests;
            return Response::json($objectSee, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function showAllUserInformation(Request $request, $friend, $id)
    {
        $me = Users::find($id);
        $friend = Users::find($friend);

        if ($me && $friend) {
            $senderUser_me  = Friends::whereRaw("user_send = ? AND state = 1", [$me->id])->with('receipt')->get();
            $receiveUSer_me = Friends::whereRaw("user_receipt = ? AND state = 1", [$me->id])->with('send')->get();

            $data_me = collect();

            foreach ($senderUser_me as $key => $value) {

                $data_me->push($value->receipt);
            }

            foreach ($receiveUSer_me as $key => $value) {
                $data_me->push($value->send);
            }

            $senderUser_friend  = Friends::whereRaw("user_send = ? AND state = 1", [$friend->id])->with('receipt')->get();
            $receiveUSer_friend = Friends::whereRaw("user_receipt = ? AND state = 1", [$friend->id])->with('send')->get();

            $data_friend = collect();

            foreach ($senderUser_friend as $key => $value) {

                $data_friend->push($value->receipt);
            }

            foreach ($receiveUSer_friend as $key => $value) {
                $data_friend->push($value->send);
            }

            $data = collect();

            foreach ($data_me as $key_me => $value_me) {
                foreach ($data_friend as $key_friend => $value_friend) {
                    if ($value_me->id == $value_friend->id) {
                        $data->push($value_friend);
                    }
                }
            }

            $friend->interests;
            $friend->similar = $data;
            return Response::json($friend, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $objectUpdate = Users::whereRaw('id=?',$id)->with('types','direcciones','membresias','referencias')->first();
        if ($objectUpdate) {
            try {
                $objectUpdate->username = $request->get('username', $objectUpdate->username);
                $objectUpdate->email = $request->get('email', $objectUpdate->email);
                $objectUpdate->firstname = $request->get('firstname', $objectUpdate->firstname);
                $objectUpdate->lastname = $request->get('lastname', $objectUpdate->lastname);
                $objectUpdate->work = $request->get('work', $objectUpdate->work);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->age = $request->get('age', $objectUpdate->age);
                $objectUpdate->birthday = $request->get('birthday', $objectUpdate->birthday);
                $objectUpdate->phone = $request->get('phone', $objectUpdate->phone);
                $objectUpdate->picture = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->last_conection = $request->get('last_conection', $objectUpdate->last_conection);
                $objectUpdate->facebook_id = $request->get('facebook_id', $objectUpdate->facebook_id);
                $objectUpdate->one_signal_id = $request->get('one_signal_id', $objectUpdate->one_signal_id);
                $objectUpdate->last_latitud = $request->get('last_latitud', $objectUpdate->last_latitud);
                $objectUpdate->last_longitud = $request->get('last_longitud', $objectUpdate->last_longitud);
                $objectUpdate->type = $request->get('type', $objectUpdate->type);
                $objectUpdate->youtube_channel = $request->get('youtube_channel', $objectUpdate->youtube_channel);
                $objectUpdate->color = $request->get('color', $objectUpdate->color);
                $objectUpdate->opcion1 = $request->get('opcion1', $objectUpdate->opcion1);
                $objectUpdate->opcion2 = $request->get('opcion2', $objectUpdate->opcion2);
                $objectUpdate->opcion3 = $request->get('opcion3', $objectUpdate->opcion3);
                $objectUpdate->color_button = $request->get('color_button', $objectUpdate->color_button);
                $objectUpdate->puntaje = $request->get('puntaje', $objectUpdate->puntaje);
                $objectUpdate->state = $request->get('state', $objectUpdate->state);
                $objectUpdate->dueno = $request->get('dueno', $objectUpdate->dueno);
                $objectUpdate->membresia = $request->get('membresia', $objectUpdate->membresia);
                $objectUpdate->inicioMembresia = $request->get('inicioMembresia', $objectUpdate->inicioMembresia);
                $objectUpdate->finMembresia = $request->get('finMembresia', $objectUpdate->finMembresia);
                $objectUpdate->tipoNivel = $request->get('tipoNivel', $objectUpdate->tipoNivel);
                $objectUpdate->nivelMembresia = $request->get('nivelMembresia', $objectUpdate->nivelMembresia);
                $objectUpdate->opcion4 = $request->get('opcion4', $objectUpdate->opcion4);
                $objectUpdate->opcion5 = $request->get('opcion5', $objectUpdate->opcion5);
                $objectUpdate->opcion6 = $request->get('opcion6', $objectUpdate->opcion6);
                $objectUpdate->opcion7 = $request->get('opcion7', $objectUpdate->opcion7);
                $objectUpdate->opcion8 = $request->get('opcion8', $objectUpdate->opcion8);
                $objectUpdate->opcion9 = $request->get('opcion9', $objectUpdate->opcion9);
                $objectUpdate->opcion11 = $request->get('opcion11', $objectUpdate->opcion11);
                $objectUpdate->opcion12 = $request->get('opcion12', $objectUpdate->opcion12);
                $objectUpdate->opcion13 = $request->get('opcion13', $objectUpdate->opcion13);
                $objectUpdate->opcion14 = $request->get('opcion14', $objectUpdate->opcion14);
                $objectUpdate->opcion15 = $request->get('opcion15', $objectUpdate->opcion15);
                $objectUpdate->opcion16 = $request->get('opcion16', $objectUpdate->opcion16);
                $objectUpdate->opcion17 = $request->get('opcion17', $objectUpdate->opcion17);
                $objectUpdate->opcion18 = $request->get('opcion18', $objectUpdate->opcion18);
                $objectUpdate->opcion19 = $request->get('opcion19', $objectUpdate->opcion19);
                $objectUpdate->opcion20 = $request->get('opcion20', $objectUpdate->opcion20);
                $objectUpdate->vencimiento = $request->get('vencimiento', $objectUpdate->vencimiento);
                $objectUpdate->referencia = $request->get('referencia', $objectUpdate->referencia);
                $objectUpdate->save();

                $objectUpdate->number_events_created = Events::where('user_created', '=', $objectUpdate->id)->count();
                $objectUpdate->number_events_assisted = UserEvents::whereRaw('user = ? AND state = 1', [$objectUpdate->id])->count();
                $objectUpdate->interests;
                return Response::json($objectUpdate, 200);
            }catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function uploadAvatar(Request $request, $id) {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('avatar', $request->avatar);

                    $objectUpdate->picture = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function uploadAvatarPic1(Request $request, $id) {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('avatars', $request->avatar);

                    $objectUpdate->pic1 = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function uploadAvatarPic2(Request $request, $id) {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('avatars', $request->avatar);

                    $objectUpdate->pic2 = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    public function uploadAvatarPic3(Request $request, $id) {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {

            $validator = Validator::make($request->all(), [
                'avatar'      => 'required|image|mimes:jpeg,png,jpg'
            ]);

            if ($validator->fails()) {
                $returnData = array(
                    'status' => 400,
                    'message' => 'Invalid Parameters',
                    'validator' => $validator->messages()->toJson()
                );
                return Response::json($returnData, 400);
            }
            else {
                try {
                    $path = Storage::disk('s3')->put('avatars', $request->avatar);

                    $objectUpdate->pic3 = Storage::disk('s3')->url($path);
                    $objectUpdate->save();

                    return Response::json($objectUpdate, 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }

            }

            return Response::json($objectUpdate, 200);
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function recoveryPassword(Request $request){
        $objectUpdate = Users::whereRaw('email=? or username=?',[$request->get('username'),$request->get('username')])->first();
        if ($objectUpdate) {
            try {
                $faker = Faker::create();
                // $pass = $faker->password(8,15,true,true);
                $pass = $faker->regexify('[a-zA-Z0-9-_=+*%@!]{8,15}');
                $objectUpdate->password = Hash::make($pass);
                $objectUpdate->state = 21;
                
                Mail::send('emails.recovery', ['empresa' => 'GTechnology', 'url' => 'http://gtechnology.gt', 'password' => $pass, 'username' => $objectUpdate->username, 'email' => $objectUpdate->email, 'name' => $objectUpdate->firstname.' '.$objectUpdate->lastname,], function (Message $message) use ($objectUpdate){
                    $message->from('info@foxylabs.gt', 'Info GTechnology')
                            ->sender('info@foxylabs.gt', 'Info GTechnology')
                            ->to($objectUpdate->email, $objectUpdate->firstname.' '.$objectUpdate->lastname)
                            ->replyTo('info@foxylabs.gt', 'Info GTechnology')
                            ->subject('Contraseña Reestablecida');
                
                });
                
                $objectUpdate->save();
                
                return Response::json($objectUpdate, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }

    public function changePassword(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
            'new_pass' => 'required|min:3',
            'old_pass'      => 'required'
        ]);

        if ($validator->fails()) {
            $returnData = array(
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator->messages()->toJson()
            );
            return Response::json($returnData, 400);
        }
        else {
            $old_pass = $request->get('old_pass');
            $new_pass_rep = $request->get('new_pass_rep');
            $new_pass =$request->get('new_pass');
            $objectUpdate = Users::find($id);
            if ($objectUpdate) {
                try {
                    if(Hash::check($old_pass, $objectUpdate->password))
                    {                       
                        if($new_pass_rep != $new_pass)
                        {
                            $returnData = array(
                                'status' => 404,
                                'message' => 'Passwords do not match'
                            );
                            return Response::json($returnData, 404);
                        }

                        if($old_pass == $new_pass)
                        {
                            $returnData = array(
                                'status' => 404,
                                'message' => 'New passwords it is same the old password'
                            );
                            return Response::json($returnData, 404);
                        }
                        $objectUpdate->password = Hash::make($new_pass);
                        $objectUpdate->state = 1;
                        $objectUpdate->save();

                        return Response::json($objectUpdate, 200);
                    }else{
                        $returnData = array(
                            'status' => 404,
                            'message' => 'Invalid Password'
                        );
                        return Response::json($returnData, 404);
                    }
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }
            }
            else {
                $returnData = array(
                    'status' => 404,
                    'message' => 'No record found'
                );
                return Response::json($returnData, 404);
            }
        }
    }
    public function deleteAvatar($id)
    {
        try{
            $objectUpdate = Users::find($id);
            if ($objectUpdate) {
                try {
                    $direcciones = explode("/",str_replace('https://','',$objectUpdate->picture));
                    $name = $direcciones[3];
                    $path = Storage::disk('s3')->delete('avatar/'.$name);
                    $objectUpdate->picture = '';
                    $objectUpdate->save();

                    return Response::json([$objectUpdate, $name], 200);
                }
                catch (Exception $e) {
                    $returnData = array(
                        'status' => 500,
                        'message' => $e->getMessage()
                    );
                }
            }

            return Response::json($objectUpdate, 200);
        }catch(Exception $e){
            $returnData = array (
                'status' => 501,
                'message' => $e->getMessage()
            );
            return Response::json($returnData, 501);
        }
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $objectDelete = Users::find($id);
        if ($objectDelete) {
            try {
                Users::destroy($id);
                return Response::json($objectDelete, 200);
            }
            catch (Exception $e) {
                $returnData = array(
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array(
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
