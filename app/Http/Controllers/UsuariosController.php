<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Support\Facades\Mail;

use App\Http\Requests;
use App\Users;
use Faker\Factory as Faker;
use Response;
use Validator;
use Hash;
use Storage;
use DB;

class UsuariosController extends Controller
{
    /**
    * Display a listing of the resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        return Response::json(Users::all(), 200);
    }
    
    public function getThisByFilter(Request $request, $id,$state)
    {
        if($request->get('filter')){
            switch ($request->get('filter')) {
                case 'state':{
                    $objectSee = Users::whereRaw('state=?',[$state])->get();
                    break;
                }
                case 'tipo':{
                    $objectSee = Users::whereRaw('tipo=?',[$state])->get();
                    break;
                }
                case 'rol':{
                    $objectSee = Users::whereRaw('rol=?',[$state])->get();
                    break;
                }
                default:{
                    $objectSee = Users::all();
                    break;
                }
    
            }
        }else{
                $objectSee = Users::all();
        }
    
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for creating a new resource.
    *
    * @return \Illuminate\Http\Response
    */
    public function create()
    {
        //
    }
    
    /**
    * Store a newly created resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @return \Illuminate\Http\Response
    */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'username'          => 'required',
            'password'          => 'required',
            'email'          => 'required',
            'firstname'          => 'required'
        ]);
        if ( $validator->fails() ) {
            $returnData = array (
                'status' => 400,
                'message' => 'Invalid Parameters',
                'validator' => $validator
            );
            return Response::json($returnData, 400);
        }
        else {
            $email = $request->get('email');
             $email_exists  = Users::whereRaw("email = ?", $email)->count();
             $user = $request->get('username');
             $user_exists  = Users::whereRaw("username = ?", $user)->count();
             if($email_exists == 0 && $user_exists == 0){    
                 try {
                     $newObject = new Users();
                     $newObject->username = $user;
                     $newObject->password = Hash::make($request->get('password'));
                     $newObject->email = $email;
                     $newObject->firstname = $request->get('firstname', '');
                     $newObject->lastname = $request->get('lastname', '');
                     $newObject->description = $request->get('description', '');
                     $newObject->birthday = $request->get('birthday', '');
                     $newObject->phone = $request->get('phone', '');;
                     $newObject->admin = $request->get('admin', 0);
                     $newObject->pic1 = $request->get('pic1', 'http://foxylabs.xyz/Documentos/imgs/Zapatos.jpg');
                     $newObject->pic2 = $request->get('pic2', 'http://foxylabs.xyz/Documentos/imgs/Category.jpg');
                     $newObject->pic3 = $request->get('pic3', 'http://foxylabs.xyz/Documentos/imgs/categories.jpg');
                     $newObject->last_conection = date('Y-m-d H:m:s');
                     $newObject->facebook_id = '';
                     $newObject->one_signal_id = $request->get('one_signal_id','');
                     $newObject->youtube_channel = $request->get('youtube_channel','');
                     $newObject->last_latitud = 0.0;
                     $newObject->last_longitud = 0.0;
                     $newObject->picture = "http://foxylabs.xyz/Documentos/imgs/logo.png";
                     $newObject->tipo = $request->get('tipo',21);
                     $newObject->opcion4 = $request->get('opcion4', null);
                     $newObject->opcion5 = $request->get('opcion5', null);
                     $newObject->opcion6 = $request->get('opcion6', null);
                     $newObject->opcion7 = $request->get('opcion7', null);
                     $newObject->opcion8 = $request->get('opcion8', null);
                     $newObject->opcion9 = $request->get('opcion9', null);
                     $newObject->opcion11 = $request->get('opcion11', null);
                     $newObject->opcion12 = $request->get('opcion12', null);
                     $newObject->opcion13 = $request->get('opcion13', null);
                     $newObject->opcion14 = $request->get('opcion14', null);
                     $newObject->opcion15 = $request->get('opcion15', null);
                     $newObject->opcion16 = $request->get('opcion16', null);
                     $newObject->opcion17 = $request->get('opcion17', null);
                     $newObject->opcion18 = $request->get('opcion18', null);
                     $newObject->opcion19 = $request->get('opcion19', null);
                     $newObject->opcion20 = $request->get('opcion20', null);
                     $newObject->save();
                    
                     $objectSee = Users::whereRaw('id=?',$newObject->id)->first();
                     if ($objectSee) {
                        Mail::send('emails.confirm', ['empresa' => 'GTechnology', 'url' => 'http://gtechnology.gt', 'app' => 'http://me.gtechnology.gt', 'password' => $request->get('password'), 'username' => $objectSee->username, 'email' => $objectSee->email, 'name' => $objectSee->firstname.' '.$objectSee->lastname,], function (Message $message) use ($objectSee){
                            $message->from('info@foxylabs.gt', 'Info GTechnology')
                                    ->sender('info@foxylabs.gt', 'Info GTechnology')
                                    ->to($objectSee->email, $objectSee->firstname.' '.$objectSee->lastname)
                                    ->replyTo('info@foxylabs.gt', 'Info GTechnology')
                                    ->subject('Usuario Creado');
                        
                        });
                         return Response::json($objectSee, 200);
                     
                     }
                     else {
                         $returnData = array (
                             'status' => 404,
                             'message' => 'No record found'
                         );
                         return Response::json($returnData, 404);
                     }
                     
                 }
                 catch(Exception $e) {
                     $returnData = array(
                         'status' => 500,
                         'message' => $e->getMessage()
                     );
                     return Response::json($returnData, 500);
                 }
             } else {
                $returnData = array (
                    'status' => 400,
                    'message' => "User already exists"
                );
                return Response::json($returnData, 400);
             }
        }
    }
    
    /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        $objectSee = Users::find($id);
        if ($objectSee) {
            return Response::json($objectSee, 200);
    
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    /**
    * Show the form for editing the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function edit($id)
    {
        //
    }
    
    /**
    * Update the specified resource in storage.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function update(Request $request, $id)
    {
        $objectUpdate = Users::find($id);
        if ($objectUpdate) {
            try {
                $objectUpdate->username = $request->get('username', $objectUpdate->username);
                $objectUpdate->email = $request->get('email', $objectUpdate->email);
                $objectUpdate->firstname = $request->get('firstname', $objectUpdate->firstname);
                $objectUpdate->lastname = $request->get('lastname', $objectUpdate->lastname);
                $objectUpdate->description = $request->get('description', $objectUpdate->description);
                $objectUpdate->birthday = $request->get('birthday', $objectUpdate->birthday);
                $objectUpdate->phone = $request->get('phone', $objectUpdate->phone);
                $objectUpdate->admin = $request->get('admin', $objectUpdate->admin);
                $objectUpdate->pic1 = $request->get('pic1', $objectUpdate->pic1);
                $objectUpdate->pic2 = $request->get('pic2', $objectUpdate->pic2);
                $objectUpdate->pic3 = $request->get('pic3', $objectUpdate->pic3);
                $objectUpdate->youtube_channel = $request->get('youtube_channel', $objectUpdate->youtube_channel);
                $objectUpdate->last_latitud = $request->get('last_latitud', $objectUpdate->last_latitud);
                $objectUpdate->last_longitud = $request->get('last_longitud', $objectUpdate->last_longitud);
                $objectUpdate->picture = $request->get('picture', $objectUpdate->picture);
                $objectUpdate->tipo = $request->get('tipo', $objectUpdate->tipo);
                $objectUpdate->opcion4 = $request->get('opcion4', $objectUpdate->opcion4);
                $objectUpdate->opcion5 = $request->get('opcion5', $objectUpdate->opcion5);
                $objectUpdate->opcion6 = $request->get('opcion6', $objectUpdate->opcion6);
                $objectUpdate->opcion7 = $request->get('opcion7', $objectUpdate->opcion7);
                $objectUpdate->opcion8 = $request->get('opcion8', $objectUpdate->opcion8);
                $objectUpdate->opcion9 = $request->get('opcion9', $objectUpdate->opcion9);
                $objectUpdate->opcion11 = $request->get('opcion11', $objectUpdate->opcion11);
                $objectUpdate->opcion12 = $request->get('opcion12', $objectUpdate->opcion12);
                $objectUpdate->opcion13 = $request->get('opcion13', $objectUpdate->opcion13);
                $objectUpdate->opcion14 = $request->get('opcion14', $objectUpdate->opcion14);
                $objectUpdate->opcion15 = $request->get('opcion15', $objectUpdate->opcion15);
                $objectUpdate->opcion16 = $request->get('opcion16', $objectUpdate->opcion16);
                $objectUpdate->opcion17 = $request->get('opcion17', $objectUpdate->opcion17);
                $objectUpdate->opcion18 = $request->get('opcion18', $objectUpdate->opcion18);
                $objectUpdate->opcion19 = $request->get('opcion19', $objectUpdate->opcion19);
                $objectUpdate->opcion20 = $request->get('opcion20', $objectUpdate->opcion20);
                $objectUpdate->save();
                return Response::json($objectUpdate, 200);
            } catch (\Illuminate\Database\QueryException $e) {
                if($e->errorInfo[0] == '01000'){
                    $errorMessage = "Error Constraint";
                }  else {
                    $errorMessage = $e->getMessage();
                }
                $returnData = array (
                    'status' => 505,
                    'SQLState' => $e->errorInfo[0],
                    'message' => $errorMessage
                );
                return Response::json($returnData, 500);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
    
    
    /**
    * Remove the specified resource from storage.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function destroy($id)
    {
        $objectDelete = Users::find($id);
        if ($objectDelete) {
            try {
                Users::destroy($id);
                return Response::json($objectDelete, 200);
            } catch (Exception $e) {
                $returnData = array (
                    'status' => 500,
                    'message' => $e->getMessage()
                );
                return Response::json($returnData, 500);
            }
        }
        else {
            $returnData = array (
                'status' => 404,
                'message' => 'No record found'
            );
            return Response::json($returnData, 404);
        }
    }
}
