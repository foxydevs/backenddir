<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Recomendaciones extends Model
{
    protected $table = 'recomendaciones';

    public function usuarios(){
        return $this->hasOne('App\Users','id','usuario');
    }

    public function recomendados(){
        return $this->hasOne('App\Comercios','id','recomendado');
    }
}
