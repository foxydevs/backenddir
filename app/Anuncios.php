<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anuncios extends Model
{
    protected $table = 'anuncios';

    public function comercios(){
        return $this->hasOne('App\Comercios','id','comercio');
    }
}
