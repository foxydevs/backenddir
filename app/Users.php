<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Users extends Authenticatable
{
    protected $table = 'usuarios';

    protected $hidden = [
        'password', 'remember_token',
    ];

    public function interests(){
        return $this->hasMany('App\UserInterest','user');
    }

    public function types(){
        return $this->hasOne('App\Users_Types','id','type');
    }

    public function membresias(){
        return $this->hasOne('App\Products','id','membresia');
    }
    public function referencias(){
        return $this->hasOne('App\Users','id','referencia');
    }
    public function direcciones(){
        return $this->hasMany('App\Direcciones','direccion','id');
    }
}
