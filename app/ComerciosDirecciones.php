<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ComerciosDirecciones extends Model
{
    protected $table = 'comercios_direcciones';
}
