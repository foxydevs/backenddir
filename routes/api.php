<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::resource('alianzas', 'AlianzasController');
Route::resource('amigos', 'AmigosController');
Route::resource('anuncios', 'AnunciosController');
Route::resource('anunciosusuarios', 'AnunciosUsuariosController');
Route::resource('categorias', 'CategoriasController');
Route::resource('comercios', 'ComerciosController');
Route::resource('comerciosdirecciones', 'ComerciosDireccionesController');
Route::resource('eventos', 'EventosController');
Route::resource('eventosusuarios', 'EventosUsuariosController');
Route::resource('intereses', 'InteresesController');
Route::resource('recomendaciones', 'RecomendacionesController');
Route::resource('roles', 'RolesController');
Route::resource('skills', 'SkillsController');
Route::resource('subcategorias', 'SubCategoriasController');
Route::resource('usuarios', 'UsuariosController');

Route::post('add/alianzas', 'AlianzasController@add');

Route::get('filter/{state}/recomendaciones/{id}', 'RecomendacionesController@getThisByFilter');
Route::get('filter/{state}/anuncios/{id}', 'AnunciosController@getThisByFilter');
Route::get('filter/{state}/alianzas', 'AlianzasController@getThisByFilter');

Route::get('usuarios/{id}/comercios', 'ComerciosController@getThisByUsuario');

Route::post('upload', 'AuthenticateController@upload');

Route::post('login', 'AuthenticateController@login');

Route::get('/user', function (Request $request) {
    return $request->user();
})->middleware('auth:api');