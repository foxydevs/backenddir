<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComerciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comercios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('foto')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->string('fondo')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->tinyInteger('state')->default(1);
            $table->tinyInteger('tipo')->default(1);
            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);

            $table->integer('categoria')->nullable()->default(null)->unsigned();
            $table->foreign('categoria')->references('id')->on('categorias')->onDelete('cascade');

            $table->integer('sub_categoria')->nullable()->default(null)->unsigned();
            $table->foreign('sub_categoria')->references('id')->on('sub_categorias')->onDelete('cascade');

            $table->integer('usuario')->nullable()->default(null)->unsigned();
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comercios');
    }
}
