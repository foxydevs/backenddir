<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateComerciosDireccionesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comercios_direcciones', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('direccion')->nullable()->default(null);
            $table->string('calle')->nullable()->default(null);
            $table->string('ciudad')->nullable()->default(null);
            $table->string('codigo')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('foto')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->string('fondo')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->tinyInteger('state')->default(1);
            $table->tinyInteger('tipo')->default(1);
            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);

            $table->integer('comercio')->nullable()->default(null)->unsigned();
            $table->foreign('comercio')->references('id')->on('comercios')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('comercios_direcciones');
    }
}
