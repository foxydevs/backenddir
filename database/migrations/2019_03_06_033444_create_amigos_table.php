<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmigosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('amigos', function (Blueprint $table) {
            $table->increments('id');
            $table->string('color')->nullable()->default(null);
            $table->string('fondo')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->tinyInteger('state')->default(0);
            $table->tinyInteger('tipo')->default(1);
            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);
            $table->date('date')->nullable()->default(null);
            $table->time('time')->nullable()->default(null);

            $table->integer('usuario_send')->nullable()->default(null)->unsigned();
            $table->foreign('usuario_send')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('usuario_recibe')->nullable()->default(null)->unsigned();
            $table->foreign('usuario_recibe')->references('id')->on('usuarios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('amigos');
    }
}
