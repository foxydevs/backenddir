<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('username');
            $table->string('password');
            $table->string('email');
            $table->string('firstname')->nullable()->default(null);
            $table->string('lastname')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->date('birthday')->nullable()->default(null);
            $table->string('phone')->nullable()->default(null);
            $table->string('youtube_channel')->nullable()->default(null);
            $table->string('picture')->default('http://foxylabs.xyz/Documentos/imgs/logo.png');
            $table->dateTime('last_conection');
            $table->string('facebook_id')->nullable()->default(null);
            $table->string('one_signal_id')->nullable()->default(null);
            $table->string('pic1')->nullable()->default('http://foxylabs.xyz/Documentos/imgs/Zapatos.jpg');
            $table->string('pic2')->nullable()->default('http://foxylabs.xyz/Documentos/imgs/Category.jpg');
            $table->string('pic3')->nullable()->default('http://foxylabs.xyz/Documentos/imgs/categories.jpg');
            $table->string('video')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->string('opcion1')->nullable()->default(null);
            $table->string('opcion2')->nullable()->default(null);
            $table->string('opcion3')->nullable()->default(null);
            $table->string('color_button')->nullable()->default(null);
            $table->boolean('admin')->default(0);
            $table->double('last_latitud',15,8)->nullable()->default(null);
            $table->double('last_longitud',15,8)->nullable()->default(null);
            $table->double('puntaje')->nullable()->default(0);
            $table->integer('dueno')->nullable()->default(0);
            $table->date('inicioMembresia')->nullable()->default(null);
            $table->date('finMembresia')->nullable()->default(null);
            $table->integer('tipoNivel')->nullable()->default(0);
            $table->integer('nivelMembresia')->nullable()->default(0);
            $table->integer('opcion4')->nullable()->default(0);
            $table->integer('opcion5')->nullable()->default(0);
            $table->integer('opcion6')->nullable()->default(0);
            $table->integer('opcion7')->nullable()->default(0);
            $table->integer('opcion8')->nullable()->default(0);
            $table->integer('opcion9')->nullable()->default(0);
            $table->integer('opcion10')->nullable()->default(0);
            $table->integer('opcion11')->nullable()->default(0);
            $table->integer('opcion12')->nullable()->default(0);
            $table->integer('opcion13')->nullable()->default(0);
            $table->integer('opcion14')->nullable()->default(0);
            $table->string('opcion15')->nullable()->default(0);
            $table->string('opcion16')->nullable()->default(0);
            $table->string('opcion17')->nullable()->default(0);
            $table->string('opcion18')->nullable()->default(0);
            $table->string('opcion19')->nullable()->default(0);
            $table->integer('opcion20')->nullable()->default(0);
            $table->date('vencimiento')->nullable()->default(null);
            $table->integer('state')->default(1);
            $table->integer('tipo')->default(1);
            $table->integer('membresia')->nullable()->default(null)->unsigned();

            $table->integer('rol')->nullable()->default(null)->unsigned();
            $table->foreign('rol')->references('id')->on('roles')->onDelete('cascade');

            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('usuarios');
    }
}
