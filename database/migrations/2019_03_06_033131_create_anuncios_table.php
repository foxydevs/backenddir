<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnunciosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anuncios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('nombre')->nullable()->default(null);
            $table->string('description')->nullable()->default(null);
            $table->string('portada')->nullable()->default(null);
            $table->string('foto1')->nullable()->default(null);
            $table->string('foto2')->nullable()->default(null);
            $table->string('foto3')->nullable()->default(null);
            $table->string('color')->nullable()->default(null);
            $table->string('fondo')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->tinyInteger('state')->default(1);
            $table->tinyInteger('tipo')->default(1);
            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);
            $table->date('date')->nullable()->default(null);
            $table->time('time')->nullable()->default(null);

            $table->integer('comercio')->nullable()->default(null)->unsigned();
            $table->foreign('comercio')->references('id')->on('comercios')->onDelete('cascade');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anuncios');
    }
}
