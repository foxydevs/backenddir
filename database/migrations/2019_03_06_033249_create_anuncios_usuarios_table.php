<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAnunciosUsuariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('anuncios_usuarios', function (Blueprint $table) {
            $table->increments('id');
            $table->string('color')->nullable()->default(null);
            $table->string('fondo')->nullable()->default(null);
            $table->string('link')->nullable()->default(null);
            $table->tinyInteger('state')->default(0);
            $table->tinyInteger('tipo')->default(1);
            $table->double('latitud',15,8)->nullable()->default(null);
            $table->double('longitud',15,8)->nullable()->default(null);
            $table->date('date')->nullable()->default(null);
            $table->time('time')->nullable()->default(null);

            $table->integer('usuario')->nullable()->default(null)->unsigned();
            $table->foreign('usuario')->references('id')->on('usuarios')->onDelete('cascade');

            $table->integer('anuncio')->nullable()->default(null)->unsigned();
            $table->foreign('anuncio')->references('id')->on('anuncios')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('anuncios_usuarios');
    }
}
